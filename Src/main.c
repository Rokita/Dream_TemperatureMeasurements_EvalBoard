/**
 ******************************************************************************
 * File Name          : main.c
 * Description        : Main program body
 ******************************************************************************
 *
 * COPYRIGHT(c) 2016 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc1;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

#define LINEAR_TEMP_CHANNEL ADC_CHANNEL_2
#define DRILLING_TEMP_CHANNEL ADC_CHANNEL_3
#define CAMERA_TEMP_CHANNEL ADC_CHANNEL_6
#define CLUTCH_TEMP_CHANNEL ADC_CHANNEL_7
#define DCDC_TEMP_CHANNEL ADC_CHANNEL_8
#define DRIVER_TEMP_CHANNEL ADC_CHANNEL_9

#define SYNCH1 0xA5
#define SYNCH2 0x5A
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);
static void MX_ADC1_Init(void);
static void MX_USART1_UART_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
typedef enum AdcMeasurement
   {
   dAdcMeasurement_LinearMotor,
   dAdcMeasurement_DrillingMotor,
   dAdcMeasurement_Camera,
   dAdcMeasurement_Clutch,
   dAdcMeasurement_DcdcConverter,
   dAdcMeasurement_Driver,
   dAdcMeasurement_LastUnused
   } AdcMeasurement_T;

uint32_t u32AdcChannel[6] =
{
LINEAR_TEMP_CHANNEL,
DRILLING_TEMP_CHANNEL,
CAMERA_TEMP_CHANNEL,
CLUTCH_TEMP_CHANNEL,
DCDC_TEMP_CHANNEL,
DRIVER_TEMP_CHANNEL
};

typedef enum FrameFields
   {
   Frame_Synch1,
   Frame_Synch2,
   Frame_Linear,
   Frame_Drilling,
   Frame_Camera,
   Frame_Clutch,
   Frame_Dcdc,
   Frame_Driver,
   Frame_Crc,
   Frame_LastUnused
   } FrameFields_T;

void vConfigureAdc(AdcMeasurement_T);
uint8_t ConvertAdcMeasurement(uint16_t);
uint8_t CalculateCrc(uint8_t *, uint8_t, uint8_t);
/* USER CODE END 0 */

int main(void)
   {

   /* USER CODE BEGIN 1 */

   /* USER CODE END 1 */

   /* MCU Configuration----------------------------------------------------------*/

   /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
   HAL_Init();

   /* Configure the system clock */
   SystemClock_Config();

   /* Initialize all configured peripherals */
   MX_GPIO_Init();
   MX_ADC1_Init();
   MX_USART1_UART_Init();

   /* USER CODE BEGIN 2 */

   uint32_t u32Buff[Frame_LastUnused] =
   {
   0
   };
   uint8_t buffer[Frame_LastUnused] =
   {
   0
   };
   /* USER CODE END 2 */

   /* Infinite loop */
   /* USER CODE BEGIN WHILE */
   HAL_GPIO_WritePin(Led2_GPIO_Port, Led2_Pin, SET);
   while(1)
      {
      /* USER CODE END WHILE */

      /* USER CODE BEGIN 3 */
      HAL_GPIO_TogglePin(Led1_GPIO_Port, Led1_Pin);
      HAL_GPIO_TogglePin(Led2_GPIO_Port, Led2_Pin);

      for(uint8_t iter = 0; iter < Frame_LastUnused; iter++)
         {
         u32Buff[iter] = 0;
         }
      buffer[Frame_Synch1] = SYNCH1;
      buffer[Frame_Synch2] = SYNCH2;

      for(uint8_t iterator = 0; iterator < 100; iterator++)
         {
         vConfigureAdc(dAdcMeasurement_LinearMotor);
         if(HAL_OK != HAL_ADC_PollForConversion(&hadc1, 100))
            {
            HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
            }
         u32Buff[Frame_Linear] += ConvertAdcMeasurement(HAL_ADC_GetValue(&hadc1));

         vConfigureAdc(dAdcMeasurement_DrillingMotor);
         if(HAL_OK != HAL_ADC_PollForConversion(&hadc1, 100))
            {
            HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
            }
         u32Buff[Frame_Drilling] += ConvertAdcMeasurement(HAL_ADC_GetValue(&hadc1));

         vConfigureAdc(dAdcMeasurement_Camera);
         if(HAL_OK != HAL_ADC_PollForConversion(&hadc1, 100))
            {
            HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
            }
         u32Buff[Frame_Camera] += ConvertAdcMeasurement(HAL_ADC_GetValue(&hadc1));

         vConfigureAdc(dAdcMeasurement_Clutch);
         if(HAL_OK != HAL_ADC_PollForConversion(&hadc1, 100))
            {
            HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
            }
         u32Buff[Frame_Clutch] += ConvertAdcMeasurement(HAL_ADC_GetValue(&hadc1));

         vConfigureAdc(dAdcMeasurement_DcdcConverter);
         if(HAL_OK != HAL_ADC_PollForConversion(&hadc1, 100))
            {
            HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
            }
         u32Buff[Frame_Dcdc] += ConvertAdcMeasurement(HAL_ADC_GetValue(&hadc1));

         vConfigureAdc(dAdcMeasurement_Driver);
         if(HAL_OK != HAL_ADC_PollForConversion(&hadc1, 100))
            {
            HAL_GPIO_TogglePin(Led3_GPIO_Port, Led3_Pin);
            }
         u32Buff[Frame_Driver] += ConvertAdcMeasurement(HAL_ADC_GetValue(&hadc1));

         HAL_Delay(1);
         }

      for(uint8_t iter = Frame_Linear; iter < Frame_Crc; iter++)
         {
         buffer[iter] = (uint8_t) (u32Buff[iter] / 100);
         }

      uint8_t value = CalculateCrc(buffer, Frame_Linear, Frame_Crc);
      buffer[Frame_Crc] = value;

      HAL_UART_Transmit(&huart1, buffer, sizeof(buffer), 100);
      }
   /* USER CODE END 3 */

   }

/** System Clock Configuration
 */
void SystemClock_Config(void)
   {

   RCC_OscInitTypeDef RCC_OscInitStruct;
   RCC_ClkInitTypeDef RCC_ClkInitStruct;
   RCC_PeriphCLKInitTypeDef PeriphClkInit;

   RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
   RCC_OscInitStruct.HSIState = RCC_HSI_ON;
   RCC_OscInitStruct.HSICalibrationValue = 16;
   RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
   RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
   RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
   if(HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
      {
      Error_Handler();
      }

   RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
   RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
   RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
   RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
   RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;
   if(HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
      {
      Error_Handler();
      }

   PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC;
   PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV2;
   if(HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
      {
      Error_Handler();
      }

   HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

   HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

   /* SysTick_IRQn interrupt configuration */
   HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
   }

/* ADC1 init function */
static void MX_ADC1_Init(void)
   {

   ADC_ChannelConfTypeDef sConfig;

   /**Common config
    */
   hadc1.Instance = ADC1;
   hadc1.Init.ScanConvMode = ADC_SCAN_DISABLE;
   hadc1.Init.ContinuousConvMode = DISABLE;
   hadc1.Init.DiscontinuousConvMode = DISABLE;
   hadc1.Init.ExternalTrigConv = ADC_SOFTWARE_START;
   hadc1.Init.DataAlign = ADC_DATAALIGN_RIGHT;
   hadc1.Init.NbrOfConversion = 1;
   if(HAL_ADC_Init(&hadc1) != HAL_OK)
      {
      Error_Handler();
      }

   /**Configure Regular Channel
    */
   sConfig.Channel = ADC_CHANNEL_2;
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if(HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }

   }

/* USART1 init function */
static void MX_USART1_UART_Init(void)
   {

   huart1.Instance = USART1;
   huart1.Init.BaudRate = 38400;
   huart1.Init.WordLength = UART_WORDLENGTH_8B;
   huart1.Init.StopBits = UART_STOPBITS_1;
   huart1.Init.Parity = UART_PARITY_NONE;
   huart1.Init.Mode = UART_MODE_TX;
   huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
   huart1.Init.OverSampling = UART_OVERSAMPLING_16;
   if(HAL_UART_Init(&huart1) != HAL_OK)
      {
      Error_Handler();
      }

   }

/** Configure pins as 
 * Analog
 * Input
 * Output
 * EVENT_OUT
 * EXTI
 */
static void MX_GPIO_Init(void)
   {

   GPIO_InitTypeDef GPIO_InitStruct;

   /* GPIO Ports Clock Enable */
   __HAL_RCC_GPIOA_CLK_ENABLE()
   ;
   __HAL_RCC_GPIOB_CLK_ENABLE()
   ;

   /*Configure GPIO pin Output Level */
   HAL_GPIO_WritePin(Led3_GPIO_Port, Led3_Pin, GPIO_PIN_RESET);

   /*Configure GPIO pin Output Level */
   HAL_GPIO_WritePin(GPIOB, Led2_Pin | Led1_Pin, GPIO_PIN_RESET);

   /*Configure GPIO pin : Led3_Pin */
   GPIO_InitStruct.Pin = Led3_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
   HAL_GPIO_Init(Led3_GPIO_Port, &GPIO_InitStruct);

   /*Configure GPIO pins : Led2_Pin Led1_Pin */
   GPIO_InitStruct.Pin = Led2_Pin | Led1_Pin;
   GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
   GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
   HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

   }

/* USER CODE BEGIN 4 */

//////////////////////////////////////////////////////////////////////////////
void vConfigureAdc(AdcMeasurement_T measurement)
//////////////////////////////////////////////////////////////////////////////
   {
   ADC_ChannelConfTypeDef sConfig;
   sConfig.Channel = u32AdcChannel[measurement];
   sConfig.Rank = 1;
   sConfig.SamplingTime = ADC_SAMPLETIME_239CYCLES_5;
   if(HAL_ADC_ConfigChannel(&hadc1, &sConfig) != HAL_OK)
      {
      Error_Handler();
      }
   HAL_ADC_Start(&hadc1);
   }

//////////////////////////////////////////////////////////////////////////////
uint8_t ConvertAdcMeasurement(uint16_t u16Measurement)
//////////////////////////////////////////////////////////////////////////////
   {
   const float Vcc = 3.3;
   const uint16_t resolution = 4096;
   const float coefficient = 0.01;
   const float zeroOffset = 0.5;

   float Voltage = (u16Measurement * Vcc) / resolution;
   float Temperature = (Voltage - zeroOffset) / coefficient;

   return (uint8_t) (Temperature + 50);
   }

//////////////////////////////////////////////////////////////////////////////
uint8_t CalculateCrc(uint8_t *buff, uint8_t first, uint8_t last)
//////////////////////////////////////////////////////////////////////////////
   {
   uint32_t u32Sum = 0;
   for(uint8_t iterator = first; iterator < last; iterator++)
      {
      u32Sum += buff[iterator];
      }
   uint8_t value = (uint8_t) ((u32Sum % 254) + 1);
   return value;
   }

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
void Error_Handler(void)
   {
   /* USER CODE BEGIN Error_Handler */
   /* User can add his own implementation to report the HAL error return state */
   while(1)
      {
      }
   /* USER CODE END Error_Handler */
   }

#ifdef USE_FULL_ASSERT

/**
 * @brief Reports the name of the source file and the source line number
 * where the assert_param error has occurred.
 * @param file: pointer to the source file name
 * @param line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
   {
   /* USER CODE BEGIN 6 */
   /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
   /* USER CODE END 6 */

   }

#endif

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
