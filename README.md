# Dream_TemperatureMeasurements_EvalBoard
Dream experiment require tests in vacuum chamber where temperature will be measured on specified components like motors, camera or DCDC converters. 

This project based on STM32F100C8T6 microcontroller with 6 MCP9700 temperature sensor with analog outputs. 

This microcontroller is programmed using St-LinkV2-1 SWD programmator (line SWCLK, SWDIO, NRST, GND are connected)

ADC_1 12-bit converter is used to convert analog value to digital. 
Channel_2 is used to measure Linear Motor Temperature 
Channel_3 is used to measure Drilling Motor Temperature 
Channel_6 is used to measure Lynx Camera Temperature 
Channel_7 is used to measure Clutch Temperature 
Channel_8 is used to measure DCDC converter Temperature 
Channel_9 is used to measure Linear Motor Driver Temperature 

USART1 is used to send measured data to PC application  - This application is available under name Dream_PomiarTemperatury_GroundStation
Embedded system only send data so just Transmit functionality is configured for this communication interface. 
USART1 configuration : 8-DataBytes , 38400 BaudRate, 1 Stop, NoParity, NoFlowControl

During measurements three Leds (Led1, Led2, Led3) are set and reset to show that system is still working



